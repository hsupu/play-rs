enum MyEnum {
    eA,
    eB,
}

type MyAlias = MyEnum;

impl MyEnum {
    fn foo(&self) -> i32 {
        // "Self::" 即 self 其类型的别名
        match self {
            Self::eA => 0x8000_0000,
            Self::eB => 0x8000_0001,
        }
    }
}
