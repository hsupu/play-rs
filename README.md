# Rust-lang Playground

- 211009 println.rs 学习基本的格式化输出
- 211009 tuple.rs 学习基本的元组写法
- 211009 enum.rs 学习基本的枚举写法、match 写法
- 211009 struct_fmt.rs 学习结构体的格式化输出、fmt::Debug
- 211009 struct.rs 学习基本的结构体写法
- 211009 slice.rs 学习基本的数组、数组切片写法
- 211009 type_alias.rs 学习基本的类型别名写法
