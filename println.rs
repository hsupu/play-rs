fn main() {
    let i = 42;
    let j = 233;
    println!("{} {}", i, j);
    println!("{1} {0}", i, j);
    println!("{a} {b}", a = i, b = j,);
}
