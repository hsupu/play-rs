use std::fmt;

// derive 使其自动继承 trait fmt::Debug 的实现
#[derive(fmt::Debug)]
struct My(i32);

impl fmt::Display for My {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // 定义为元组的结构体，其元素通过 ".<index>" 访问
        // 最后一条表达式 "return <expr>;" 可简写为 "<expr>" 去掉 "return" 和 ";"
        write!(f, "my={}", self.0)
    }
}

//TODO impl fmt::Binary for My

fn main() {
    // println! 用法示例
    let my = My(42);
    println!("my = {:?}", my);  // debug
    println!("my = {:#?}", my); // pretty debug
    println!("my = {}", my);    // display
}
