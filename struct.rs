use std::fmt;

// 单元（unit）结构体
struct Nil;

// 元组结构体
#[derive(std::Debug)]
struct MyTuple(i32, i32);

impl fmt::Display for MyTuple {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // 通过 ".<index>" 访问
        write!(f, "({}, {})", self.0, self.1)
    }
}

// 带有字段（field）的结构体
#[derive(std::Debug)]
struct MyStruct {
    x: i32,
    y: i32,
}

impl fmt::Display for MyStruct {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // 通过定义的字段名访问
        write!(f, "x:{}, y:{}", self.x, self.y)
    }
}

fn main() {
    // 创建元组结构体
    let myt = MyTuple(1, 2);
    println!("myt = {?:}", myt);
    println!("myt = {}", myt);

    // 创建字段结构体
    let mys = MyStruct { x: 1, y: 2, };
    println!("mys = {?:}", mys);
    println!("mys = {}", mys);

    // 复用 mys 的其他字段
    let mys2 = MyStruct { x: 3, ..mys };

    // 创建单元结构体
    let nil = Nil;

    // 解构 myt 得到变量 my_a my_b
    let MyTuple(my_a, my_b) = myt;

    // 解构 mys 得到变量 my_x my_y
    let MyStruct { x: my_x, y: my_y, } = mys;
}