fn main() {
    // 一元组
    let one = (42,);
    // 二元组（不同类型）
    let pair = (1, true);
    // 使用元组的元素
    let key = pair.0;
    // 解构（deconstruct）
    let (first, second) = pair;
    // 嵌套元组
    let embedded = ((1,),);
}
