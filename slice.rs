/**
 * 数组（array）是具有相同类型的、定长的、连续的对象容器
 * 类型标记是 [T; size]
 *
 * 切片（slice）是具有相同类型的、编译时期不确定长度的、连续的对象的指针，有两个 usize 长，一是指针，二是长度
 * 类型标记是 &[T]
 */
use std::mem;

// 借用一个切片
fn dump_slice(slice: &[i32]) {
    println!("lens = {}", slice.len());
}

fn main() {
    // 定长数组
    let fixed: [i32; 5] = [1, 2, 3, 4, 5];
    println!("fixed len={}", fixed.len());
    println!("fixed sz={}", mem::size_of_val(&fixed));
    println!("fixed 1st={}", fixed[0]);

    // 数组被借用为指针
    dump_slice(&fixed);
    // [include..exclude]
    dump_slice(&fixed[1..4]);

    // 编译期越界检查
    // println!("{}", fixed[5]);
}
