// 枚举（enum）是限定了范围的类型
// 任何合法的 struct 都是合法的 enum 项
//
#![allow(dead_code)]

// 使用隐式辨别值（implicit discriminator），从 0 开始
enum MyImplicit {
    eA,
    eB,
}

// 使用显式辨别值
enum MyExplicit {
    eA = 4,
    eB = 2,
}

enum WebEvent {
    Open(String),
    Close,
    Click{ x: i64, y: i64 },
}

fn inspect(event: WebEvent) {
    match event {
        WebEvent::Open(url) => println!("open {}", url),
        WebEvent::Close => println!("close"),
        WebEvent::Click{ x, y } => println!("click {} {}", x, y),
    }
}

fn main() {
    inspect(WebEvent::Open("http://localhost:3000/".to_owned()));
    inspect(WebEvent::Click{ x: 0, y: 0});
    inspect(WebEvent::Close);
}
